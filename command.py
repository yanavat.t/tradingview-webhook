import json
from helpers.coinGecko import getPrice
from helpers.firebase import newUser, getUsers, updateUser


from actions import line_single_push_message, reply_message

HELP_TXT = "!help"
SUBSCRIBE_TEXT = "!subscribe"
UNSUBSCRIBE_TEXT = "!unsubscribe"


def help_message(payload):
    replyMessage = "คำสั่ง \nพิมพ์ {} : ติดตามสัญญาณ Super Trend  \nพิมพ์ {} : ยกเลิกการติดตาม".format(
        SUBSCRIBE_TEXT, UNSUBSCRIBE_TEXT)
    replyToken = payload['replyToken']
    reply_message(replyToken, replyMessage)


def handle_subscribe(payload, isSubscribed):
    if isSubscribed:
        newUser(payload)
        replyMessage = 'ติดตามสัญญาณ Super Trend แล้ว 🚀🚀 '
        replyToken = payload['replyToken']
        reply_message(replyToken, replyMessage)
    else:
        updateUser(payload, isSubscribed)
        replyMessage = 'ยกเลิกติดตามสัญญาณ Super Trend แล้ว  🙅‍♂️🙅‍♂️'

        # no replyToken on unfollow event
        if not 'replyToken' in payload:
            replyToken = payload['source']['userId']
            line_single_push_message(replyToken, replyMessage)
        else:
            replyToken = payload['replyToken']
            reply_message(replyToken, replyMessage)


def handle_unsubscribe(payload):
    newUser(payload)
    replyMessage = 'ยกเลิกติดตามสัญญาณ Super Trend แล้ว  🙅‍♂️🙅‍♂️'
    replyToken = payload['replyToken']
    reply_message(replyToken, replyMessage)


def handle_price(payload):
    message = payload['message']['text']
    replyToken = payload['replyToken']
    textMessage = ""
    if 'btc'.upper() in message.upper():
        textMessage = getPrice('bitcoin')
    elif 'eth'.upper() in message.upper():
        textMessage = getPrice('ethereum')
    elif 'sol'.upper() in message.upper():
        textMessage = getPrice('solana')
    strResponse = textMessage and 'price : {} usd\nusd_24h_change : {:f}'.format(
        textMessage['usd'], textMessage['usd_24h_change'])

    textMessage and reply_message(replyToken, strResponse)


def handle_command(cmd, payload):
    switcher = {
        HELP_TXT: lambda: help_message(payload),
        SUBSCRIBE_TEXT: lambda: handle_subscribe(payload, True),
        UNSUBSCRIBE_TEXT: lambda: handle_subscribe(payload, False),
    }
    func = switcher.get(cmd, lambda: handle_price(payload))
    return func()
