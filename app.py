
from actions import telegram_bot_sendtext, line_notify_sendtext, line_push_message

from flask import Flask, render_template, request, abort
from command import SUBSCRIBE_TEXT, UNSUBSCRIBE_TEXT, handle_command
import config
import json
import requests
app = Flask(__name__)


@app.route("/")
def dashboard():
    # not imprement yet!!
    return render_template('dashboard.html', alpaca_orders=orders)


@app.route("/linehook", methods=['POST'])
def linehook():
    if request.method != 'POST':
        abort(400)
    print(request.json)

    reqJson = request.json
    payload = reqJson['events'][0]
    # line_push_message('test')  # test multicast line push

    # events follow
    if payload['type'] == 'follow':
        handle_command(SUBSCRIBE_TEXT, payload)
        return request.json
    # events unfollow
    if payload['type'] == 'unfollow':
        handle_command(UNSUBSCRIBE_TEXT, payload)
        return request.json

    # events msg
    if payload['type'] == 'message' and payload['message']['type'] == "text":
        command = payload['message']['text'].lower()
        handle_command(command, payload)
        return request.json
    else:
        abort(400)


@app.route("/webhook", methods=['POST'])
def webhook():
    webhook_message = json.loads(request.data)
    price = webhook_message['strategy']['order_price']
    quantity = webhook_message['strategy']['order_contracts']
    symbol = webhook_message['ticker']
    side = webhook_message['strategy']['order_action']
    print(webhook_message)

    if not webhook_message['passphrase']:
        abort(400)

    timeFrame = ""
    if (webhook_message['passphrase']) == "ST_4HR":
        timeFrame = "4HR"
    elif (webhook_message['passphrase']) == "ST_1HR":
        timeFrame = "1HR"
    else:
        timeFrame = webhook_message['passphrase']

    #

    # prepare msg
    iconSide = '🟢' if str(side) == "buy" else '🔴'
    notifyMessage = (iconSide + ' ' + str(side).upper() + '\n' +
                     ' Signal : price :' + str(price) + '\n' +
                     ' quantity : ' + '{0:.8f}'.format(quantity) + '\n' +
                     ' symbol : ' + str(symbol) + '\n' +
                     ' timeframe : ' + str(timeFrame)
                     )
    telegram_bot_sendtext(notifyMessage)
    line_push_message(notifyMessage)

    return webhook_message
