import requests
import config
import json
from helpers.firebase import getActiveUsers

# define param
channel_access_token = config.LINE_TOKEN


def line_notify_sendtext(bot_message):
    send_text = bot_message
    header = {'Authorization': 'Bearer ' + channel_access_token}

    url = 'https://api.line.me/v2/bot/message/broadcast'
    msg = {
        "messages": [
            {
                "type": "text",
                "text": send_text
            },
        ]
    }

    response = requests.post(url, json=msg, headers=header)
    return response.json()


def telegram_bot_sendtext(bot_message):
    bot_token = config.TELEGRAM_TOKEN
    bot_chatID = config.TELEGRAM_CID
    send_text = 'https://api.telegram.org/bot' + bot_token + \
        '/sendMessage?chat_id=' + bot_chatID + '&parse_mode=Markdown&text=' + bot_message
    # print(send_text)
    response = requests.get(send_text)

    return response.json()


def do_line_push_message(toUser, textMessage):
    print(toUser, textMessage)
    LINE_API = 'https://api.line.me/v2/bot/message/multicast'
    Authorization = 'Bearer {}'.format(channel_access_token)
    # print(Authorization)
    headers = {
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': Authorization
    }

    data = {
        "to": toUser,
        "messages": [{
            "type": "text",
            "text": textMessage
        }]
    }

    data = json.dumps(data)  # dump dict >> Json Object
    r = requests.post(LINE_API, headers=headers, data=data)
    return 200


def line_single_push_message(toUser, textMessage):
    if toUser:
        return do_line_push_message(list({toUser}, textMessage))


def line_push_message(textMessage):
    users = getActiveUsers()
    print(users)
    if users:
        return do_line_push_message(users, textMessage)


def reply_message(Reply_token, TextMessage):
    LINE_API = 'https://api.line.me/v2/bot/message/reply'

    Authorization = 'Bearer {}'.format(channel_access_token)
    # print(Authorization, Reply_token)
    headers = {
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': Authorization
    }

    data = {
        "replyToken": Reply_token,
        "messages": [{
            "type": "text",
            "text": TextMessage
        }]
    }

    data = json.dumps(data)  # dump dict >> Json Object
    r = requests.post(LINE_API, headers=headers, data=data)
    return 200
