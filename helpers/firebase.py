import pyrebase
from config import firebaseConfig
firebase = pyrebase.initialize_app(firebaseConfig)
db = firebase.database()


def noquote(s):
    return s


# fix bug on pyrebase 3 library
pyrebase.pyrebase.quote = noquote


def getUsers():
    resultList = []
    if db.child('users').shallow().get().val():
        users = db.child('users').get()

        for user in users.each():
            resultList.append(user.val()["userId"])
    return resultList


def getActiveUsers():
    resultList = []
    if db.child('users').get().val():
        users = db.child('users').order_by_child('status').equal_to(True).get()

        for user in users.each():
            resultList.append(user.val()["userId"])
    return resultList


def getUserByUserId(userId):
    # getUser by id
    res = db.child('users').order_by_child(
        "userId").equal_to(userId).get()
    return list(res.each())


def checkDuplicateUsers(userId):
    # check duplicate user on db
    if db.child('users').get().val():
        isDuplicateUsers = getUserByUserId(userId)
        print('isDuplicateUsers', isDuplicateUsers)
        return len(isDuplicateUsers) > 0
    else:
        # No users DB
        return False


def newUser(data):
    source = data['source']
    if checkDuplicateUsers(source['userId']):
        print('duplicated user ', source['userId'])
        #  duplicated subscribe then update user status true
        updateUser(data, True)
        return data
    else:
        objData = {"userId": source['userId'],
                   "type": source['type'], "status": True}
        print('new user :', objData)
        db.child('users').push(objData)
        return objData


def updateUser(data, status):
    source = data['source']
    userList = getUserByUserId(source['userId'])
    userRef = userList[0].key()

    # set new status to user
    updateUser = {"userId": source['userId'],
                  "type": source['type'], "status": status}
    db.child('users').child(userRef).set(updateUser)

    return data


def deleteUser(data):
    # not imprement yet
    print(data)
    return data
