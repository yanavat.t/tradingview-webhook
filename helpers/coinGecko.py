from pycoingecko import CoinGeckoAPI

cg = CoinGeckoAPI()


def getPrice(coin):

    if cg.ping():
        result = cg.get_price(ids=coin, vs_currencies='usd', include_market_cap=True,
                              include_24hr_vol=True, include_24hr_change=True, include_last_updated_at=True)

        return result[coin]
